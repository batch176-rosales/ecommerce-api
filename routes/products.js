//Dependencies and Modules
const express = require ('express');
const ProductController = require ('../controller/products');
const auth = require ('../auth');


//[SECTION] Routing Component
const route = express.Router(); 

const {verify, verifyAdmin} = auth;

//[SECTION] Routes for creating a product
route.post('/create',verify, verifyAdmin, (req, res) => {
    ProductController.addProduct(req.body).then(result => res.send(result));
});

//[SECTION] Routes- GET all products
route.get('/all', verify, verifyAdmin, (req, res) => {
    ProductController.getAllProducts().then(result => res.send(result));
});

//Retrieve all ACTIVE courses
route.get('/active', (req, res) => {
	ProductController.getAllActive().then(result => res.send(result));
});


//Retrieving a SPECIFIC product
route.get('/:productId', (req, res) => {
	//console.log(req.params.productId)
	ProductController.getProduct(req.params.productId).then(result => res.send(result));
});

//[SECTION] Routes for Updating a Product
route.put('/:productId', verify, verifyAdmin, (req, res) => {
    ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result))
});

//Archiving a Product
route.put('/:productId/archive', verify, verifyAdmin, (req, res) => {ProductController.archiveProduct(req.params.productId).then(result => res.send(result));
})


//[SECTION] Route for Deleting a Product
route.delete('/delete',verify, verifyAdmin, (req, res) => {
ProductController.deleteProduct(req.body.productId).then(result => res.send(result))
})
   
  


//[SECTION] Expose Route System
module.exports = route;