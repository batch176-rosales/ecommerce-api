//[SECTION] Dependencies and Modules
const exp = require('express');
const controller = require('../controller/users'); 
const auth = require ('../auth');

//[SECTION] Routing Component
const route = exp.Router(); 

const {verify, verifyAdmin} = auth;

//[SECTION] Routes for Registration
route.post('/register', (req, res) => {
   // console.log(req.body);
    let userData = req.body;
    controller.register(userData).then(outcome => {
    res.send(outcome);
   })
}); 

//[SECTION] Route for User Authentication(login)
route.post('/login',(req, res) => {
controller.loginUser(req.body).then(result => res.send(result));
});


//[SECTION] Routes- GET the user's details
route.get('/details', auth.verify, (req, res) => {
controller.getProfile(req.user.id).then(result => res.send(result));
});


//[STRETCH GOAL]: Set User as Admin
route.put('/:userId/update', verify, verifyAdmin, (req, res) => {controller.updateUser(req.params.userId).then(result => res.send(result));
})



//[SECTION] Expose Route System
    module.exports = route; 




