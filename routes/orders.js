//Dependencies and Modules
const express = require ('express');
const OrderController = require ('../controller/orders');
const auth = require ('../auth');


//[SECTION] Routing Component
const route = express.Router(); 

const {verify, verifyAdmin} = auth;


//[SECTION] Routes for creating an order
 route.post('/create',verify, (req, res) => {
    OrderController.createOrder(req.body).then(result => res.send(result));
}); 
 
//STRETCH GOAL: GET <Retrieve AUTHENTICATED User's Orders>
route.get('/userOrder',verify, (req,res) => {
    OrderController.getUserOrder(req.body.orderId).then(result => res.send(result));
});


//STRETCH GOAL: GET <Retrieve ALL Orders [Admin Only]
route.get('/allOrders', verify, verifyAdmin, (req,res) => {
    OrderController.getAllOrders().then(result => {return res.send(result)});
})


//[SECTION] Expose Route System
module.exports = route;