//[SECTION] Dependencies and Modules
const User = require('../models/User');
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');
const auth = require('../auth');

//[SECTION] Environment Variables Setup
  dotenv.config(); 
  const salt = Number(process.env.SALT); 
  

//[SECTION] Functionalities [CREATE] New Account
  module.exports.register = (userData) => {
	  let email = userData.email;
	  let password = userData.password; 
	  
	  let newUser = new User({
		  email: email,
		  password: bcrypt.hashSync(password, salt),
	  }); 
	 	
	  return newUser.save().then((user, err) => {
		  
		  if (user) {
			  return user; 	
		  } else {
			  return 'Failed to Register account'; 
		  }; 
	  }); 
  }; 

  //[SECTION] User Authentication
 	 module.exports.loginUser = (data) => {
	 return User.findOne({email: data.email }).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)
			if(isPasswordCorrect){
				return { accessToken: auth.createAccessToken(result.toObject())}
			}else{
				return false;
			
			}
		};
	});
  };


//[SECTION] Functionalities [RETRIEVE]
	module.exports.getProfile = (data) => {
	return User.findById(data).then(result =>{
		// change the value of the user's password to an empty string
		result.password = '';
		return result;
	})
	};


//[SECTION] Functionalities [UPDATE] | [STRETCH GOAL]
	module.exports.updateUser = (userId) => {
    let updatedActiveField = {
        isAdmin: true
    };
    return User.findByIdAndUpdate(userId, updatedActiveField).then((user, error) =>{
        if (error){
            return false;
        } else{
            return true;
        }
    }).catch (error => error)
}



