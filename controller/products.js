//Dependencies and Modules
const Product = require ('../models/Product');

//[SECTION] Functionalities [CREATE] Product
module.exports.addProduct = (reqBody) => {
    let newProduct = new Product ({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    });
    return newProduct.save().then((product,error) =>{
        if(error){
            return false;
        }else {
            return true;
        }
    }).catch(error => error)
};

//[SECTION] Functionalities [RETRIEVE] All Products
module.exports.getAllProducts = () => {
    return Product.find({}).then(result => {
        return result;
    }).catch(error => error)
};

//Retrieve all ACTIVE products
module.exports.getAllActive = () => {
    return Product.find({isActive: true}).then(result => {
        return result;
    }).catch (error => error)
};

//Retrieving a SPECIFIC product
module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams).then(result =>{
        return result
    }).catch(error => error)
};

//[SECTION] Functionalities [UPDATE]
module.exports.updateProduct = (productId, data) => {
    let updatedProduct = {
        name: data.name,
        description: data.description,
        price: data.price
    }
    return Product.findByIdAndUpdate(productId, updatedProduct).then ((product, error) => {
        if(error){
            return false;
        }else{
            return true;
        }
    }).catch(error => error)

};

//Archiving a Product
module.exports.archiveProduct = (productId) => {
    let updatedActiveField = {
        isActive: false
    };
    return Product.findByIdAndUpdate(productId, updatedActiveField).then((product, error) =>{
        if (error){
            return false;
        } else{
            return true;
        }
    }).catch (error => error)
}

//[SECTION] - Delete a Product
module.exports.deleteProduct = (reqBody) => {
    return Product.findByIdAndRemove(reqBody).then((fulfilled, rejected) => {
        if (fulfilled) {
            return `The Product has been successfully Removed.`;
        } else {
            return `Failed to remove Product.`;
            
        };

    });
};
