//Dependencies and Modules
const Order = require ('../models/Order');

//[SECTION] Functionalities [CREATE] Order
 module.exports.createOrder = (reqBody) => {
      let newOrder = new Order ({
        userId: reqBody.userId,
        products:[{
            productId: reqBody.productId,
            quantity: reqBody.quantity 
        }],
        price: reqBody.price,
        totalAmount: reqBody.price * reqBody.quantity
   }); 
    return newOrder.save().then((order,error) => {
    if(error){
        return false;
        
    }else {
            return newOrder;
        }
    }).catch(error => error)
};  

//[SECTION] Functionalities [RETRIEVE] | STRETCH GOAL
 module.exports.getUserOrder = (reqBody) => {
    return Order.findById(reqBody).then(result =>{
        return result
    }).catch(error => error.message)
};

//[SECTION] Functionalities [RETRIEVE ALL] | STRETCH GOAL
module.exports.getAllOrders = () => {
    return Order.find({}).then(result => {
        return result;
    }).catch(error => error.message)
}














//Retrieve all ACTIVE orders
 module.exports.getAllActive = () => {
    return Order.find({isActive: true}).then(result => {
        return result;
    }).catch (error => error)
} 


//UPDATE  an order
/*  module.exports.updateUser = (orderId, data) => {
    let updatedOrder = {
       userId: reqBody.userId,
        productId: reqBody.productId,
        quantity: reqBody.quantity
    }
    
    return Order.findByIdAndUpdate(orderId, updatedOrder).then ((order, error) => {
        if(error){
            return false;
        }else{
            return true;
        }
    }).catch(error => error)

} */ 

