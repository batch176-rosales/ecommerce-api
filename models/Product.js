//[SECTION] Modules and Dependencies
const mongoose = require ('mongoose');


//[SECTION] Schema/Blueprint
const productSchema = new mongoose.Schema({
   
    name: {
        type: String,
        required: [true, 'Product Name is Required']
    },
    description: {
        type: String,
        required: [true, 'Product Descrption is Required']
    },
    price: {
        type: Number,
        required: [true, 'Product Price is Required']
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    productId: {
        type: Object
    }
   /*  orders: [
        {
            orderId: {
                type: String,
                required: [true, 'Order ID is Required']
            }
        }
    ] */
})
   

//[SECTION] Model
    module.exports = mongoose.model('Product', productSchema);